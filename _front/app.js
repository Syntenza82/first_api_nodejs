// Modules
const bodyParser = require('body-parser')
const express = require('express')
const morgan = require('morgan')('dev')
const twig = require('twig')

// Variables globales
const app = express()
const port = 8081

// Middlewares
app.use(morgan)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Routes
app.get('/:name', (req, res) => {
    res.render('index.twig', {
        name: req.params.name
    })
    //express method => res.sendFile(__dirname + '/views/index.html')
})

// Lancement de l'application
app.listen(port, () => console.log('Started on port ' + port))